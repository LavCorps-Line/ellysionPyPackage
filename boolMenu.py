# Standard Library Imports #
import asyncio
import contextlib
import functools
import string
from typing import (
    Callable,
    ClassVar,
    Iterable,
    List,
    Optional,
    Pattern,
    Sequence,
    Tuple,
    Union,
    cast,
)

# Third-Party Imports #
import discord
from redbot.core import commands
from redbot.core.utils.chat_formatting import (
    bordered,
    box,
    escape,
    format_perms_list,
    humanize_list,
)
from redbot.core.utils.embed import randomize_color
from redbot.core.utils.menus import (
    DEFAULT_CONTROLS,
    close_menu,
    menu,
    next_page,
    prev_page,
    start_adding_reactions,
)
from redbot.core.utils.predicates import MessagePredicate, ReactionPredicate


class boolMenuPredicate(Callable[[discord.Message], bool]):
    """Predicate class for boolMenu"""

    def __init__(self, predicate: Callable[["boolMenuPredicate", discord.Message], bool]) -> None:
        self._pred: Callable[["boolMenuPredicate", discord.Message], bool] = predicate
        self.result = None

    def __call__(self, message: discord.Message) -> bool:
        return self._pred(self, message)

    @classmethod
    def boolContained_in(
        cls,
        collection: Sequence[str],
        ctx: Optional[commands.Context],
        channel: Optional[discord.TextChannel] = None,
        user: Optional[discord.abc.User] = None,
    ) -> "boolMenuPredicate":
        same_context = MessagePredicate.same_context(ctx, channel, user)

        def predicate(self: boolMenuPredicate, message: discord.Message) -> bool:
            if not same_context(message):
                return False
            for command in message.content:
                if command in collection:
                    return True
            return False

        return cls(predicate)


class boolMenuUtils:
    """Utilities class for boolMenu"""

    def boolValidCommands(inputCommands: Sequence[str], validCommands: Sequence[str]):
        validatedInput = []
        for command in inputCommands:
            if command in validCommands:
                validatedInput.append(command)
        return validatedInput

    def boolMapValues(data: list):

        alphabetReal = list(string.ascii_lowercase)

        alphabetDisplay = [
            "🇦",
            "🇧",
            "🇨",
            "🇩",
            "🇪",
            "🇫",
            "🇬",
            "🇭",
            "🇮",
            "🇯",
            "🇰",
            "🇱",
            "🇲",
            "🇳",
            "🇴",
            "🇵",
            "🇶",
            "🇷",
            "🇸",
            "🇹",
            "🇺",
            "🇻",
            "🇼",
            "🇽",
            "🇾",
            "🇿",
        ]
        controls = []
        y = 0

        for dat in data:
            mapping = {}

            mapping["variable"] = dat["variable"]
            mapping["display"] = alphabetDisplay[y]
            mapping["character"] = alphabetReal[y]

            controls.append(mapping)

            y += 1

        # controls.append(
        #    {
        #        "variable"=None,
        #        "display"=alphabetDisplay[-1],
        #        "character"=alphabetReal[-1]
        #    }
        # )

        return controls

    def boolGenDisplay(name: str, data: list, controls: list):
        trueChar = "✅"
        falseChar = "❌"

        displayDescriptionLst = []
        displayDescriptionStr = ""

        for iter in len(data):
            displayDescriptionLst.append(
                data[iter]["name"]
                + "(:regional_indicator_"
                + controls[iter]["character"]
                + ":) = "
                + trueChar
                if data[iter]["variable"]
                else falseChar
            )

        for line in displayDescriptionLst:
            displayDescriptionStr = displayDescriptionStr.join(line) + "\n"

        display = randomize_color(
            discord.Embed(title=name + " Config", description=displayDescriptionStr)
        )

        return display


class boolMenu:
    """Base class for boolMenu"""

    def __init__(
        self,
        ctx: commands.Context,
        name: str,
        data: list,
        message: discord.Message = None,
        timeout: float = 30.0,
    ):
        self.ctx = ctx
        self.bot = ctx.bot
        self.controls = boolMenuUtils.boolMapValues(data)
        self.message = message
        self.timeout = timeout
        self.data = data
        self.display = boolMenuUtils.boolGenDisplay(name, data, controls)
        self.response = None
        self.name = name

    async def update(self):
        self.display = display
        try:
            await message.edit(embed=display)
            return True
        except:
            return False

    async def waitFunction(self):
        try:
            message = await self.bot.wait_for(
                "message",
                # check=MessagePredicate.contained_in(tuple(controls.keys()), self.ctx),
                check=boolMenuPredicate.reContained_in(tuple(controls.keys()), self.ctx),
                timeout=self.timeout,
            )
        except asyncio.TimeoutError:
            self.response = None
            return
        else:
            self.response = message
            for i in range(0, len(self.data)):
                if self.controls[i]["character"] in message.content:
                    self.data[i]["function"](not self.data[i][variable])
            cont = await self.update()
            if not cont:
                return False
