# Standard Library Imports #
import datetime
import math
import random
import secrets

# Third-Party Imports #
import discord
import humanize
import requests
import sympy
from redbot.core import commands
from redbot.core.checks import *
from redbot.core.utils import chat_formatting
from redbot.core.utils.embed import randomize_color
from redbot.core.utils.menus import (
    DEFAULT_CONTROLS,
    close_menu,
    menu,
    next_page,
    prev_page,
    start_adding_reactions,
)

# First-Party Imports #
from ellysion import memIo


class iterUtils:
    def isIter(toCheck):
        try:
            toCheck.__iter__
        except:
            return False
        else:
            return True


class listUtils:
    def twoDimList(x: list, y: list):
        """two dimensional menu pages initializer"""
        return {"x": x, "y": y}

    def twoListDiffs(before, after):
        if not iterUtils.isIter(before) or not iterUtils.isIter(after):
            return
        contrastDict = {"add": [], "remove": []}
        for x in after:
            if x not in before:
                contrastDict["add"].append(x)
        for x in before:
            if x not in after:
                contrastDict["remove"].append(x)
        return contrastDict


class compUtils:
    def deepCompare(before, after):
        beforeDir = dir(before)
        afterDir = dir(before)

        beforeDict = {}
        for entry in beforeDir:
            beforeDict[entry] = exec("before.{}".format(entry))
        afterDict = {}
        for entry in afterDict:
            afterDict[entry] = exec("after.{}".format(entry))

        contrastDict = {"add": [], "change": [], "remove": []}


class mathUtils:
    def isPrimeNumber(number: int):
        if not number > 1:
            return False  # Numbers less than or equal to 1 are guaranteed to not be prime numbers.
        for integer in range(2, number):
            if (
                number % integer
            ) == 0:  # Is the remainder of division equal to 0 (i.e. cleanly divided)?
                return False  # is not a prime number.
        else:
            return True  # is a prime number!

    def xIsPrimeNumber(number: int):  # Expensive!
        if not number > 1:
            raise ValueError(
                "Prime number check should be performed on numbers greater than 1."
            )  # simple check to ensure we don't waste CPU time on a useless operation (i.e. 10 digit negative numbers)
        cleanDivList = []  # init list of clean divisors
        for integer in range(1, number + 1):  # begin loop
            if number / integer == int(
                number / integer
            ):  # if input divided by range's integer is equal to the non-decimal version of the input divided by the range's integer...
                cleanDivList.append(
                    integer
                )  # then it was a clean division, and we should add the integer to the cleanDivList.
        if len(cleanDivList) == 2:  # if number is divisible by 1 and itself...
            return True  # it's a prime number!
        else:
            cleanDivList.remove(1)  # get rid of this simple value
            cleanDivList.remove(number)  # this too
            return (
                cleanDivList
            )  # return list of cleanly divisible numbers. reciving code should perform type checking to ensure error-free execution of code.

    def percFormula(
        num, den, perc
    ):  # Calculate percent formula using x/y=z/100 where x = % of y and z = % of x relative to y. thanks, Marocco :)
        solvMode = None  # False for truth testing, True for solving
        if sympy.symbol.Symbol in [type(num), type(den), type(perc)]:
            solvMode = True
        for nmb in [num, den, perc]:
            if type(nmb) == int:
                nmb = sympy.Rational(nmb, 1)  # cast pyInt to sympyInt
            elif type(nmb) == float:
                nmb = sympy.Rational(nmb)  # cast pyFloat to sympyRational
        return [num, den, perc]


class datetimeUtils:
    def wfAPIparseDate(apiTimeObject: str):
        return datetime.datetime.strptime(apiTimeObject, "%Y-%m-%dT%H:%M:%S.%fZ")

    def td_format(td_object):
        seconds = int(td_object.total_seconds())
        periods = [
            ("year", 60 * 60 * 24 * 365),
            ("month", 60 * 60 * 24 * 30),
            ("day", 60 * 60 * 24),
            ("hour", 60 * 60),
            ("minute", 60),
            ("second", 1),
        ]

        strings = []
        for period_name, period_seconds in periods:
            if seconds > period_seconds:
                period_value, seconds = divmod(seconds, period_seconds)
                has_s = "s" if period_value > 1 else ""
                strings.append("%s %s%s" % (period_value, period_name, has_s))

        return ", ".join(strings)


class messageUtils:
    def embedifyMessage(message):
        embed = discord.Embed(description=message.content)
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url)
        embed.set_footer(text="{}".format(str(message.id)))
        embed.timestamp = message.created_at
        return embed


class strUtils:
    def remove_text_inside_brackets(text, brackets="()[]"):
        count = [0] * (len(brackets) // 2)  # count open/close brackets
        saved_chars = []
        for character in text:
            for i, b in enumerate(brackets):
                if character == b:  # found bracket
                    kind, is_close = divmod(i, 2)
                    count[kind] += (-1) ** is_close  # `+1`: open, `-1`: close
                    if count[kind] < 0:  # unbalanced bracket
                        count[kind] = 0  # keep it
                    else:  # found bracket to remove
                        break
            else:  # character is not a [balanced] bracket
                if not any(count):  # outside brackets
                    saved_chars.append(character)
        return "".join(saved_chars)
