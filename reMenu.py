# Standard Library Imports #
import asyncio
import contextlib
import functools
import re
from typing import (
    Callable,
    ClassVar,
    Iterable,
    List,
    Optional,
    Pattern,
    Sequence,
    Tuple,
    Union,
    cast,
)

# Third-Party Imports #
import discord
from redbot.core import commands
from redbot.core.utils.chat_formatting import (
    bordered,
    box,
    escape,
    format_perms_list,
    humanize_list,
)
from redbot.core.utils.menus import (
    DEFAULT_CONTROLS,
    close_menu,
    menu,
    next_page,
    prev_page,
    start_adding_reactions,
)
from redbot.core.utils.predicates import MessagePredicate, ReactionPredicate


def controlMapper(commandCharacters: list, characterFunctions: list):
    if not len(commandCharacters) == len(characterFunctions):
        raise TypeError(
            "commandCharacters and characterFunctions should be the same length\nLength of commandCharacters: "
            + str(len(commandCharacters))
            + "\nLength of characterFunctions:"
            + str(len(characterFunctions))
        )
    return dict(zip(commandCharacters, characterFunctions))


class reMenuPredicate(Callable[[discord.Message], bool]):
    """Predicate class for reMenu"""

    def __init__(self, predicate: Callable[["reMenuPredicate", discord.Message], bool]) -> None:
        self._pred: Callable[["reMenuPredicate", discord.Message], bool] = predicate
        self.result = None

    def __call__(self, message: discord.Message) -> bool:
        return self._pred(self, message)

    @classmethod
    def reContained_in(
        cls,
        collection: Sequence[str],
        ctx: Optional[commands.Context],
        channel: Optional[discord.TextChannel] = None,
        user: Optional[discord.abc.User] = None,
    ) -> "reMenuPredicate":
        same_context = MessagePredicate.same_context(ctx, channel, user)

        def predicate(self: reMenuPredicate, message: discord.Message) -> bool:
            if not same_context(message):
                return False
            for command in message.content:
                if command in collection:
                    return True
            return False

        return cls(predicate)


class reMenuUtils:
    """Utilities class for reMenu"""

    def reValid_commands(inputCommands: Sequence[str], validCommands: Sequence[str]):
        validatedInput = []
        for command in inputCommands:
            if command in validCommands:
                validatedInput.append(command)
        return validatedInput


class reMenu:
    """Base class for reMenu"""

    def __init__(
        self,
        ctx: commands.Context,
        controls: dict,
        data: dict,
        display: discord.Embed,
        message: discord.Message = None,
        timeout: float = 30.0,
    ):
        self.ctx = ctx
        self.bot = ctx.bot
        self.controls = controls
        self.message = message
        self.timeout = timeout
        self.data = data
        self.display = display
        self.response = None

        for key, value in controls.items():
            maybe_coro = value
            if isinstance(value, functools.partial):
                maybe_coro = value.func
            if not asyncio.iscoroutinefunction(maybe_coro):
                raise RuntimeError("Function must be a coroutine")

    async def update(self, display=None, data=None, message=None):
        if display:
            if data:
                self.data = data
            self.display = display
            if not message:
                message = await ctx.send(embed=display)
                return True
            else:
                await message.edit(embed=display)
                return True
        else:
            return False

    async def waitFunction(self):
        try:
            message = await self.bot.wait_for(
                "message",
                # check=MessagePredicate.contained_in(tuple(controls.keys()), self.ctx),
                check=reMenuPredicate.reContained_in(tuple(controls.keys()), self.ctx),
                timeout=self.timeout,
            )
        except asyncio.TimeoutError:
            self.response = None
        else:
            self.response = message
            return await controls[self.response.content](
                ctx, controls, message, timeout, data, display, response
            )
