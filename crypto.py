"""
DISCLAIMER:

Do not, for the love of god, use this to encrypt important or private data.
This is simply an experiment in combining ciphers and encoding.
"""

# Standard Library Imports #
import base64


class base:
    def enRot47(plaintext: str, key: int = 47):
        """ROT47 with adjustable key size across the entire range of valid ASCII. Intended for use with LCROT64."""
        ciphertext = []  # init list of characters modified by enRot47
        for iteration in range(
            len(plaintext)
        ):  # for number in list of numbers defined by the number of characters in the plaintext input...
            ascPos = ord(plaintext[iteration])  # get ascii position of plaintext letter i
            if ascPos >= 0 and ascPos <= 127:  # if valid ascii...
                ciphertext.append(
                    chr((ascPos + key) % 127)
                )  # shift with key, wrapping to valid ascii range
            else:  # if not valid ascii...
                ciphertext.append(plaintext[i])  # just slap it back in!
        return "".join(ciphertext)

    def deRot47(ciphertext: str, key: int):
        """convenience function reversing a ROT47 cipher"""
        return base.enRot47(ciphertext, -key)

    def enRot13(plaintext: str, key: int = 13):
        """ROT13 with adjustable key size for A-Z."""
        for char in plaintext:
            cipherdict = {}  # init list of characters modified by enRot13
            for character in (65, 97):  # for valid ascii in range 65-97 (a-z A-Z)...
                for iteration in range(26):
                    cipherdict[chr(iteration + character)] = chr(
                        (iteration + key) % 26 + character
                    )  # cipher character and apply to cipherdict
        return "".join(
            [cipherdict.get(character, character) for character in plaintext]
        )  # return ciphered text

    def deRot13(ciphertext: str, key: int = 13):
        """convenience function reversing a ROT13 cipher"""
        return base.enRot13(ciphertext, -key)

    def enFrobnicate(plaintext: str, key: int = 42):
        """memfrob (linux) style cipher"""
        ciphertext = ""  # init list of characters modified by enFrobnicate
        for character in plaintext:
            ciphertext += chr(ord(character) ^ key)  # xor character by key, append to ciphertext
        return ciphertext

    def deFrobnicate(ciphertext: str, key: int = 42):
        """convenience function reversing a Frobnicate cipher"""
        return base.enFrobnicate(ciphertext, key)

    def enRot524288(s):
        y = ""
        for x in s:
            # y += chr(ord(x) ^ 0x80000)
            y += chr(ord(x) ^ 524288)
        return y

    def deRot524288(s):
        return base.enRot524288(s)

    def enRot32768(s):
        y = ""
        for x in s:
            # y += chr(ord(x) ^ 0x8000)
            y += chr(ord(x) ^ 32768)
        return y

    def deRot32768(s):
        return base.enRot32767(s)

    def enUniRot(plaintext, key):
        ciphertext = ""
        for character in plaintext:
            ciphertext += chr(ord(character) ^ key)
        return ciphertext

    def deUniRot(ciphertext, key):
        return base.enUniRot(ciphertext, key)


class extended:
    """LavCorps Ciphers"""

    def enRot64(plaintext: str, keys: list):
        """
        ROT64 is a very simple, and therefore likely very weak cipher.
        It works by:
            1. applying a ROT47 encipherment to the plaintext
            2. encoding the result in base64
            3. enciphering the final base64 encoded string iteration in ROT47 with the first key.
        """
        ciphertext = plaintext
        for iteration in range(len(keys)):
            ciphertext = base.enRot47(ciphertext, keys[iteration])
            ciphertext = base64.b64encode(ciphertext.encode()).decode()
        if iteration == range(len(keys))[-1]:
            ciphertext = base.enRot47(ciphertext, keys[0])
        return ciphertext

    def deRot64(ciphertext: str, keys: list):
        "convenience function reversing a ROT64 cipher"
        plaintext = ciphertext
        plaintext = base.deRot47(plaintext, keys[0]).encode()
        for iteration in range(len(keys)):
            plaintext = base64.b64decode(plaintext).decode()
            plaintext = base.deRot47(plaintext, keys[0 - (iteration + 1)]).encode()
        return plaintext.decode()

    def enFrob64(plaintext: str, keys: list):
        """
        Frob64 is a variant of ROT64.
        It works by:
            1. Applying a Frobnicate encipherment to the plaintext
            2. encoding the result in base64
            3. enciphering the final base64 encoded string iteration with a Frobnicate with the first key.
        """
        ciphertext = plaintext
        for iteration in range(len(keys)):
            ciphertext = base.enFrobnicate(ciphertext, keys[iteration])
            ciphertext = base64.b64encode(ciphertext.encode()).decode()
        if iteration == range(len(keys))[-1]:
            ciphertext = base.enFrobnicate(ciphertext, keys[0])
        return ciphertext

    def deFrob64(ciphertext: str, keys: list):
        "convenience function reversing a ROT64 cipher"
        plaintext = ciphertext
        plaintext = base.deFrobnicate(plaintext, keys[0]).encode()
        for iteration in range(len(keys)):
            plaintext = base64.b64decode(plaintext).decode()
            plaintext = base.deFrobnicate(plaintext, keys[0 - (iteration + 1)]).encode()
        return plaintext.decode()
