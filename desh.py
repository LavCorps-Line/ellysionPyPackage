# Standard Library Imports #
import asyncio
import subprocess


class desh:
    async def run(cmd):
        proc = await asyncio.create_subprocess_shell(
            cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
        )

        stdout, stderr = await proc.communicate()

        return [proc, stdout, stderr]
