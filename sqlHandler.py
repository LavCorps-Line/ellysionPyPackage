# Standard Library Imports #
import json
import random

# Third-Party Imports #
import mysql.connector


class sqlHandler:
    def __init__(self):
        self.db = mysql.connector.connect(
            host="localhost", user="ellysion", passwd="OTdZMVV6eXpUcjNsZjZ2Sg", database="ellysion"
        )

        self.cursors = {}
        self.validTypes = [type([]), type({})]

    def validateInput(self, input: type):
        if type(input) != type:
            raise ValueError("validateInput input should be class 'type'")
        for dataType in self.validTypes:
            if input == dataType:
                return True
        return False
